var orderReceived = document.getElementById("orderReceived")
var orderItem = document.getElementById("orderItem")
var viewBtn = document.getElementById('view_btn')
var closeBtn = document.getElementById('close-btn')

// table link
var orderListTable = document.getElementById("orderConfirmedTable");

window.onload = () => {
  fetch("/orders/accepted")
    .then(res => res.json())
    .then(data => {
      data.message.forEach(element => {
        displayOrders(element)
      });
    }).catch(e => console.log(e))
}

function displayOrders(orders) {
    var orderListTable = document.getElementById("orderConfirmedTable");
    var tableBody = orderListTable.querySelector("tbody"); // Get reference to the table body

    var row = tableBody.insertRow(orderListTable.rows.length -1)
    row.id = `${orders._id}`;

    var td = []
    for (var i=0; i<orderListTable.rows[0].cells.length; i++){
      td[i] = row.insertCell(i);
    } 
    var rowIndex = tableBody.rows.length;

    td[0].innerHTML = rowIndex;    ;
    td[1].innerHTML = orders.email;
    td[2].innerHTML = '<input type="button" onclick="showOrderDetails(this)" value="View" id="view_btn"></input>'
}

function showOrderDetails(event) {
    const rowId = event.parentNode.parentNode.id
    const rowIndex = event.parentNode.parentNode.rowIndex

    fetch(`/order/specific/${rowId}`)
      .then(res => res.json())
      .then(data => {
        const order = data.message
        console.log(order);
        orderItem.innerHTML = `
        <span id="close-icon" onclick="closeOrderDetails(this)">&times;</span>
        <h4><strong>Order</strong></h4>
        <hr class="custom_line">
        <p><strong>Quantity : </strong>${order.quantity}</p>
        <p><strong>Date : </strong>${order.date}</p>
        <p><strong>Time : </strong>${order.time}</p>
        <p><strong>Phone No : </strong>${order.number}</p>
        <p><strong>Description : </strong>${order.description}</p>
        <button id="rejectBtn" onclick="deleteOrder(${rowIndex}, '${rowId}')">Finished</button>
    `;
      }).catch(e => console.log(e))
    

    orderReceived.style.visibility = "visible";
    orderReceived.style.opacity = "1"
}

function closeOrderDetails(){
  orderReceived.style.visibility = "hidden";
  orderReceived.style.opacity = "0";
} 

function deleteOrder(rowIndex,rowId) {
  if (confirm("Are you sure you have completed the order?")) {
    fetch(`/order/status/${rowId}/?status=completed`,{method:"PUT"}).then(res => {
      if(res.ok){
        orderReceived.style.visibility = "hidden";
        orderReceived.style.opacity = "0";
        orderListTable.deleteRow(rowIndex);
      }
    }).catch(e => console.log(e))
  }
}