function logout() {
    
    fetch("/student/logout",{
        method:"POST"
    }).then(res => {
        if (res.ok){
            window.open("Index.html","_self")
        }
    }).catch(e => console.log(e))
}

function adminLogout() {
    fetch("/admin/logout",{method:"POST"})
        .then(res => {
            if (res.ok){
                window.open("Index.html","_self")
            }
        })
        .catch(e => console.log(e))
}