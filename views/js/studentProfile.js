var passwordInput = document.getElementById('new_password')
var confirmPasswordInput = document.getElementById('confirmNew_password')
var form = document.getElementById("setting-form")

var emailField = document.getElementById("email")
var enrollmentField = document.getElementById("enrollment")

window.onload = () => {
    fetch("/student")
    .then(res => res.json())
    .then(data => {
        emailField.innerHTML = data.message.email
        enrollmentField.innerHTML = data.message.enrollment
    })
    .catch(e => console.log(e))
}

    const validateForm = () => {
        let isPasswordValid = checkPassword(),
            isConfirmPasswordValid = checkConfrimPassword();
        let isFormValid = isPasswordValid && isConfirmPasswordValid;

         if (isFormValid){
            fetch("/student/passwordReset",{
                method:"PUT",
                headers:{'Content-Type':'application/json; charset=UTF-8'},
                body: JSON.stringify({password:passwordInput.value})
            }).then((res) => {
                console.log(res.ok);
                if (res.ok){
                    alert("Password Changed")
                    return
                }else{
                    throw ""
                }
            }).catch((e) => console.log(e))
         }
         resetForm();
    }

    var passwordError = document.getElementById('newPw_Error');
    var confirmPasswordError = document.getElementById('confirmPw_Error');

    passwordInput.addEventListener('input',function(){
        checkPassword();
    })
    confirmPasswordInput.addEventListener('input', function(){
        checkConfrimPassword();
    })

    const isRequired = value => value.trim() === ''? false : true;
    // const isRequired = value => !value;

    const checkPassword = () => {
        if (!isRequired(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password cannot be blank.';
            return false;
        } else if(!isPasswordValid(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
            return false;
        } else {
            passwordInput.classList.add('is-valid');
            passwordInput.classList.remove('is-invalid');
            passwordError.innerHTML = '';
            return true;
        }
    }
    const isPasswordValid = (password) => {
        const re = new
        RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        return re.test(password);
    }

    checkConfrimPassword = () => {
        if (!isRequired(confirmPasswordInput.value)){
            confirmPasswordInput.classList.add('is-invalid');
            confirmPasswordInput.classList.remove('is-valid');
            confirmPasswordError.innerHTML = 'Please enter the password again.';
            return false;
        } else if (confirmPasswordInput.value !== passwordInput.value){
            confirmPasswordInput.classList.add('is-invalid');
            confirmPasswordInput.classList.remove('is-valid');
            confirmPasswordError.innerHTML = 'The password does not match.';
            return false;
        } else {
            confirmPasswordInput.classList.add('is-valid');
            confirmPasswordInput.classList.remove('is-invalid');
            confirmPasswordError.innerHTML = '';
            return true;
        }
    }

    function resetForm() {
        document.getElementById('new_password').value='';
        document.getElementById('confirmNew_password').value='';
    }