// Get container and arrow buttons
const container = document.querySelector('.card-slider-container');
const arrowLeft = document.querySelector('.arrow-left');
const arrowRight = document.querySelector('.arrow-right');

// Set initial card index and calculate card width
let cardIndex = 0;
const cardWidth = document.querySelector('.card').offsetWidth + parseInt(getComputedStyle(document.querySelector('.card')).marginRight);

// Add click event listener to arrow buttons
arrowLeft.addEventListener('click', slideLeft);
arrowRight.addEventListener('click', slideRight);

function slideLeft() {
  // Decrement card index and update container position
  if (cardIndex > 0) {
    cardIndex--;
    container.style.transform = `translateX(-${cardIndex * cardWidth}px)`;
  }
}

function slideRight() {
  // Increment card index and update container position
  if (cardIndex < document.querySelectorAll('.card').length - 3) {
    cardIndex++;
    container.style.transform = `translateX(-${cardIndex * cardWidth}px)`;
  }
}



