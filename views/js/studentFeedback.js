var subjectInput = document.getElementById('subject');
var descriptionInput = document.getElementById('myTextBox')

    const validateForm = () => {
        let isSubjectValid = checkSubject(),
            isDescriptionValid = checkDescription();
        let isFormValid = isSubjectValid && isDescriptionValid;

        if (isFormValid){
            // SubmitEvent;
            const data = {
                subject:subjectInput.value,
                body:descriptionInput.value
            }
            
            fetch("/feedback/create",{
                method:"POST",
                headers:{'Content-Type':'application/json;charset=UTF-8'},
                body:JSON.stringify(data)
            })
            .then((res)=> {
                if (res.ok){
                    alert("Feedback send successfully")
                    resetform()
                    return
                }
                else{
                    throw ""
                }
            }).catch((e) => {console.log(e);})
        } 
    }

    var subjectError = document.getElementById('subjectError');
    var descriptionError = document.getElementById('descriptionError');

    subjectInput.addEventListener('input', function(){
        checkSubject()
    })
    descriptionInput.addEventListener('input',function(){
        checkDescription();
    })

    const checkSubject = () => {
        if (!isRequired(subjectInput.value)){
            subjectInput.classList.add('is-invalid');
            subjectInput.classList.remove('is-valid');
            subjectError.innerHTML = 'Subject cannot be blank.';
            return false;
        } 
        else if (!isSubjectValid(subjectInput.value)){
            subjectInput.classList.add('is-invalid');
            subjectInput.classList.remove('is-valid');
            subjectError.innerHTML = 'Subject should be between 3 and 50 characters.';
            return false;
        } 
        else{
            subjectInput.classList.add('is-valid');
            subjectInput.classList.remove('is-invalid');
            subjectError.innerHTML = '';
            return true;
        }
    }
    const isSubjectValid = () => {
        if (subjectInput.value.length >= 3 && subjectInput.value.length <= 50) {
            return true;
        }
        return false;
    }
    
    const isRequired = value => value.trim() === ''? false : true;
    const checkDescription = () => {
        if (!isRequired(descriptionInput.value)){
            descriptionInput.classList.add('is-invalid');
            descriptionInput.classList.remove('is-valid');
            descriptionError.innerHTML = 'Description cannot be blank';
            return false;
        } 
        else if(!isDescriptionValid(descriptionInput.value)){
            descriptionInput.classList.add('is-invalid');
            descriptionInput.classList.remove('is-valid');
            descriptionError.innerHTML = 'Description should be between 3 and 300 characters';
            return false;
        }
         else {
            descriptionInput.classList.add('is-valid');
            descriptionInput.classList.remove('is-invalid');
            descriptionError.innerHTML = '';
            return true;
        }
    }
    const isDescriptionValid = () => {
        if (descriptionInput.value.length >= 3 && descriptionInput.value.length <= 300) {
            return true;
        }
        return false;
    }

    function resetform(){
        document.getElementById("subject").value = "";
        document.getElementById("myTextBox").value = "";
    }
    