var postCollection = [];

window.onload = () => {
    fetch("/announcements/all")
        .then(res => res.json())
        .then(data => {
            data.message.forEach(p => {
                postCollection.push({
                    subject:p.subject,
                    body:p.body
                })
            })
            // Check if admin posts are available and display them
            if (postCollection.length > 0) {
                displayAdminPosts();
            } else {
                // No admin posts available, hide the section
                var titleContainer = document.getElementById("title");
                titleContainer.style.display = "none";
            }
        })
        .catch(e => console.log(e))
}
   
// Function to display admin posts
function displayAdminPosts() {
    var fieldContainer = document.getElementById("field");
    fieldContainer.innerHTML = ""; // Clear existing content
    
    postCollection.forEach(function(post) {
        var fieldDiv = document.createElement("div");
        fieldDiv.innerHTML = `
            <div>
                <h3 class="pt-4 pb-2">Admin</h3>
            </div>
            <div>
                <hr>
                <h6>Subject:</h6>
                <p>${post.subject}</p>
                <h6>Message:</h6>
                <p class="pb-3">${post.body}</p>
            </div>
        `;
        fieldContainer.appendChild(fieldDiv);
    });
}