var emailInput = document.getElementById('email');
var passwordInput = document.getElementById('password')
var form = document.getElementById("login-form")

const validateForm = () => {
    let isEmailValid = checkEmail(),
        isPasswordValid = checkPassword();
    let isFormValid = isEmailValid && isPasswordValid;

    if (isFormValid){
        const userData = {
            email:emailInput.value,
            password: passwordInput.value
        }

        fetch(`/student/login`,{
            method:'POST',
            headers:{'Content-Type' : 'application/json; charset=UTF-8'},
            body:JSON.stringify(userData)
        })
        .then((res) => {
            return {
                status: res.status,
                data : res.json()
            }
        })
        .then((res2) => {
            const status = res2.status
            const data = res2.data

            if(status === 200){
                window.location.href = "./home.html"
            }
            else{
                data.then((errData) => {
                    alert(errData.message)
                })
            }
        })
        .catch((e) => {alert(e);})  
    }
}
         
var emailInput = document.getElementById('email');
var passwordInput = document.getElementById('password');
var emailError = document.getElementById('emailError');
var passwordError = document.getElementById('passwordError');

emailInput.addEventListener('input', function(){
    checkEmail()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})

const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[0-9]{8}\.gcit@rub\.edu\.bt$/;
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}
// const isRequired = value => value === ''? false : true;
// const isRequired = value => value === '' || value === null || typeof value === 'undefined' ? false : true;
const isRequired = value => value.trim() === ''? false : true;
// const isRequired = value => !value;

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else if(!isPasswordValid(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
const isPasswordValid = (password) => {
    const re = new
    RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
}