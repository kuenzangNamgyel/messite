var passwordInput = document.getElementById('password')
var confirmPasswordInput = document.getElementById('confirm_password')
var form = document.getElementById("rewriting_pw")

var queryString = window.location.search;
var urlParams = new URLSearchParams(queryString)
var token = urlParams.get(`token`)

    const validateForm = () => {
        let isPasswordValid = checkPassword(),
            isConfirmPasswordValid = checkConfrimPassword();
        let isFormValid = isPasswordValid && isConfirmPasswordValid;

         if (isFormValid){
            fetch(`/student/passwordResetForm/${token}`,{
                method:'POST',
                headers: {'Content-Type' : 'application/json; charset=UTF-8'},
                body: JSON.stringify({password:passwordInput.value}),
            }).then((res) => {
                return {
                    data: res.json(),
                    status:res.status
                }
             
            }).then(({data,status}) => {
                if (status === 200) {
                    data.then((err) => {
                        alert(err.message)
                    })
                    window.location.href = "studentLogin.html";
                     
                }else{
                    data.then((err) => {
                        alert(err.message)
                    })
                }
            }).catch(e => {
                console.log(e);
            }) 
         }
    }

    var passwordInput = document.getElementById('password');
    var confirmPasswordInput = document.getElementById('confirm_password');

    var passwordError = document.getElementById('passwordError');
    var confirmPasswordError = document.getElementById('confirm_passwordError');

    passwordInput.addEventListener('input',function(){
        checkPassword();
    })
    confirmPasswordInput.addEventListener('input', function(){
        checkConfrimPassword();
    })

    const isRequired = value => value.trim() === ''? false : true;
    // const isRequired = value => !value;

    const checkPassword = () => {
        if (!isRequired(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password cannot be blank.';
            return false;
        } else if(!isPasswordValid(passwordInput.value)){
            passwordInput.classList.add('is-invalid');
            passwordInput.classList.remove('is-valid');
            passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
            return false;
        } else {
            passwordInput.classList.add('is-valid');
            passwordInput.classList.remove('is-invalid');
            passwordError.innerHTML = '';
            return true;
        }
    }
    const isPasswordValid = (password) => {
        const re = new
        RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        return re.test(password);
    }

    checkConfrimPassword = () => {
        if (!isRequired(confirmPasswordInput.value)){
            confirmPasswordInput.classList.add('is-invalid');
            confirmPasswordInput.classList.remove('is-valid');
            confirmPasswordError.innerHTML = 'Please enter the password again.';
            return false;
        } else if (confirmPasswordInput.value !== passwordInput.value){
            confirmPasswordInput.classList.add('is-invalid');
            confirmPasswordInput.classList.remove('is-valid');
            confirmPasswordError.innerHTML = 'The password does not match.';
            return false;
        } else {
            confirmPasswordInput.classList.add('is-valid');
            confirmPasswordInput.classList.remove('is-invalid');
            confirmPasswordError.innerHTML = '';
            return true;
        }
    }
