var emailInput = document.getElementById('email')
var enrollInput = document.getElementById('enroll')
var passwordInput = document.getElementById('password')
var confirmPasswordInput = document.getElementById('confirm_password')
var form = document.getElementById("signUp-form")

const validateForm = () => {
    let isEmailValid = checkEmail(),
         isEnrollValid = checkEnroll(),
         isPasswordValid = checkPassword(),
         isConfirmPasswordValid = checkConfrimPassword();
     let isFormValid = isEmailValid && isEnrollValid && isPasswordValid && isConfirmPasswordValid;

     if (isFormValid){
        const userData = {
            enrollmentKey : enrollInput.value,
            email : emailInput.value,
            password : confirmPasswordInput.value
        }

        fetch(`/student/signup`,{
            method:'POST',
            headers:{'Content-Type' : 'application/json; charset=UTF-8'},
            body:JSON.stringify(userData)
        })
        .then((res) => {
            return {
                status:res.status,
                data:res.json()
            }
        })
        .then((res2) => {
            const status = res2.status
            const data = res2.data

            
            if (status === 200){

                data.then((value) => {
                    sessionStorage.setItem("signupEmail",userData.email)
                    sessionStorage.setItem("message",value.message)
                })

                window.location.href = "./studentVerify_signup.html"
            }
            else{
                data.then((errData) => {
                    if(errData.message.code === 11000){
                        alert("Duplicate credential")
                    }else{
                        alert("Check Credential")
                    }
                })
            }
        })
        .catch((e) => {alert(e)})
     }
}
     
var emailInput = document.getElementById('email');
var enrollInput = document.getElementById('enroll');
var passwordInput = document.getElementById('password');
var confirmPasswordInput = document.getElementById('confirm_password');

var emailError = document.getElementById('emailError');
var enrollError = document.getElementById('enrollError')
var passwordError = document.getElementById('passwordError');
var confirmPasswordError = document.getElementById('confirm_passwordError');

emailInput.addEventListener('input', function(){
    checkEmail()
})
enrollInput.addEventListener('input', function(){
    checkEnroll()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})
confirmPasswordInput.addEventListener('input', function(){
    checkConfrimPassword();
})

const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[0-9]{8}\.gcit@rub\.edu\.bt$/;
    // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}

const isRequired = value => value.trim() === ''? false : true;
// const isRequired = value => !value;

const checkEnroll = () => {
    if (!isRequired(enrollInput.value)){
        enrollInput.classList.add('is-invalid');
        enrollInput.classList.remove('is-valid');
        enrollError.innerHTML = 'Enrollment number cannot be blank.';
        return false;
    } else if(!isEnrollValid(enrollInput.value)){
        enrollInput.classList.add('is-invalid');
        enrollInput.classList.remove('is-valid');
        enrollError.innerHTML = 'Enrollment number is not valid';
        return false;
    } else {
        enrollInput.classList.add('is-valid');
        enrollInput.classList.remove('is-invalid');
        enrollError.innerHTML = '';
        return true;
    }
}
const isEnrollValid = (enroll) => {
    const re = /^1{1}2{1}\d{6}$/
    return re.test(enroll);
}

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else if(!isPasswordValid(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password must has at least 8 characters that include atleast 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
const isPasswordValid = (password) => {
    const re = new
    RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    return re.test(password);
}

checkConfrimPassword = () => {
    if (!isRequired(confirmPasswordInput.value)){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'Please enter the password again.';
        return false;
    } else if (confirmPasswordInput.value !== passwordInput.value){
        confirmPasswordInput.classList.add('is-invalid');
        confirmPasswordInput.classList.remove('is-valid');
        confirmPasswordError.innerHTML = 'The password does not match.';
        return false;
    } else {
        confirmPasswordInput.classList.add('is-valid');
        confirmPasswordInput.classList.remove('is-invalid');
        confirmPasswordError.innerHTML = '';
        return true;
    }
}