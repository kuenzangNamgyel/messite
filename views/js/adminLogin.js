var usernameInput = document.getElementById('username');
var passwordInput = document.getElementById('password')
var form = document.getElementById("adminLogin-form")

const validateForm = () => {
    let isUsernameValid = checkUsername(),
        isPasswordValid = checkPassword();
    let isFormValid = isUsernameValid && isPasswordValid;

    if (isFormValid){
        const adminData = {
            userName : usernameInput.value,
            password : passwordInput.value
        }

        // fetch(`/admin/login`,{
        //     method:'POST',
        //     headers: {'Content-Type' : 'application/json; charset=UTF-8'},
        //     body: JSON.stringify(adminData),
        // })
        // .then((res) => { 
        //     if (res.ok){
        //         window.location.href = "dashboard.html"
        //     }else{
        //         throw ""
        //     }
        // })
        // .catch((e) => {alert(e)})

        fetch(`/admin/login`,{
            method:'POST',
            headers: {'Content-Type' : 'application/json; charset=UTF-8'},
            body: JSON.stringify(adminData),
        })
        .then((res) => { 
            return {
                status:res.status,
                data:res.json()
            }
        })
        .then((res2)  => {
            const status = res2.status
            const data = res2.data

            if (status === 200){
                window.location.href = "./dashboard.html"
            }else{
                data.then((errData) => {
                    alert(errData.message)
                })
            }
        })
        .catch((e) => {alert(e)})
    }
}
         
usernameInput.addEventListener('input', function(){
    checkUsername()
})
passwordInput.addEventListener('input',function(){
    checkPassword();
})

const checkUsername = () => {
    if (!isRequired(usernameInput.value)){
        usernameInput.classList.add('is-invalid');
        usernameInput.classList.remove('is-valid');
        usernameError.innerHTML = 'Username cannot be blank.';
        return false;
    } 
    else{
        usernameInput.classList.add('is-valid');
        usernameInput.classList.remove('is-invalid');
        usernameError.innerHTML = '';
        return true;
    }
}
    // const isEmailValid = (email) => {
    //     const re = /^[0-9]{8}\.gcit@rub\.edu\.bt$/;
    //     // const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    //     return re.test(email)
    // }
 
const isRequired = value => value.trim() === ''? false : true;

const checkPassword = () => {
    if (!isRequired(passwordInput.value)){
        passwordInput.classList.add('is-invalid');
        passwordInput.classList.remove('is-valid');
        passwordError.innerHTML = 'Password cannot be blank.';
        return false;
    } else {
        passwordInput.classList.add('is-valid');
        passwordInput.classList.remove('is-invalid');
        passwordError.innerHTML = '';
        return true;
    }
}
    // const isPasswordValid = (password) => {
    //     const re = new
    //     RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    //     return re.test(password);
    // }
