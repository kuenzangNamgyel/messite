var emailInput = document.getElementById('email');

    const validateForm = () => {
        let isEmailValid = checkEmail();
        let isFormValid = isEmailValid;

        if (isFormValid) {
           
            fetch(`/admin/passwordRecovery`,{
                method:"POST",
                headers:{'Content-Type':'application/json; charset=UTF-8'},
                body: JSON.stringify({email:emailInput.value}),
            })
            .then((res) => {
                if (res.ok){
                    return res.json()
                }
                else if (res.status === 404) {
                    throw "Invalid Email"
                }
                else{
                    throw ""
                }
            })
            .then((data) => {
                // Session Storage
                sessionStorage.setItem("EMAIL",data.email)

                // Redirect
                window.location.href = "../admin_verify.html";
            })
            .catch((e) => {
                alert(e)
            })
        }
    }


    var emailInput = document.getElementById('email');
    var emailError = document.getElementById('emailError');

    emailInput.addEventListener('input', function(){
        checkEmail()
    })

    const checkEmail = () => {
        const email = emailInput.value.trim()
        if (!isRequired(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email cannot be blank.';
            return false;
        } else if (!isEmailValid(email)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email is not valid.';
            return false;
        } else{
            emailInput.classList.add('is-valid');
            emailInput.classList.remove('is-invalid');
            emailError.innerHTML = '';
            return true;
        }
    }
    
    const isRequired = value => value.trim() === ''? false : true;

    const isEmailValid = (email) => {
        const re = /^[0-9]{8}\.gcit@rub\.edu\.bt$/;
        return re.test(email)
    }