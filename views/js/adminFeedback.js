// POP-UP Links
var feedbackReceived = document.getElementById("feedbackReceived")
var feedbackItem = document.getElementById("feedbackItem")
var viewBtn = document.getElementById('view_btn')
var closeBtn = document.getElementById('close-btn')

// TABLE Links
const table = document.getElementById("feedbackTable")

window.onload = () => {
    fetch("/feedbacks")
        .then(res => res.json())
        .then(data => {
            data.message.forEach((data2) => displayFeedback(data2))
        })
}

// DISPLAY USER EMAIL
function displayFeedback(feedback) {
    var table = document.getElementById("feedbackTable"); // Get reference to the table
    var tableBody = table.querySelector("tbody"); // Get reference to the table body
  
    var row = tableBody.insertRow(tableBody.rows.length); // Insert a new row at the end
    row.id = feedback._id

    // Create an array to hold the cells
    var cells = [];
  
    for (var i = 0; i < table.rows[0].cells.length; i++) {
      cells[i] = row.insertCell(i); // Insert a new cell into the row
    }
    var rowIndex = tableBody.rows.length;
    
    cells[0].innerHTML = rowIndex; // Increment the Sl.No for each row
    cells[1].innerHTML = feedback.senderEmail;
    cells[2].innerHTML = '<input type="button" onclick="showFeedbackDetails(this)" value="View" id="view_btn">';
}

function showFeedbackDetails(event) {

    const rowId = event.parentNode.parentNode.id
    const rowIndex = event.parentNode.parentNode.rowIndex

    fetch(`/feedback/${rowId}`)
        .then(res => res.json())
        .then(data => {
            const feedbackData = data.message
            feedbackItem.innerHTML = `
                <h4><strong>Feedback</strong></h4>
                <hr class="custom_line">
                <p><strong>Subject : </strong>${feedbackData.subject}</p>
                <p><strong>Body : </strong>${feedbackData.body}</p>
                <button id="backBtn" class="mb-3" onclick="closeFeedback()">Back</button>
                <button id="deleteBtn" onclick="deleteFeedback(${rowIndex}, '${rowId}')">Delete</button>
            `;
        })
        .catch(e => console.log(e))

    feedbackReceived.style.visibility = "visible";
    feedbackReceived.style.opacity = '1'

}

function closeFeedback(){
  feedbackReceived.style.visibility = "hidden";
  feedbackReceived.style.opacity = "0";
} 

function deleteFeedback(rowIndex,rowId) {
    if (confirm("Are you sure you want to delete this feedback?")) {
        fetch(`/feedback/${rowId}`,{method:"DELETE"})
            .then(res => {
                if(res.ok){
                    table.deleteRow(rowIndex);
                    feedbackReceived.style.visibility = "hidden";
                    feedbackReceived.style.opacity = "0";
                }
            })
            .catch(e => console.log(e))
    }
}