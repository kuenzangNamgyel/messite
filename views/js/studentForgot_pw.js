var emailInput = document.getElementById('email');

    const validateForm = () => {
        let isEmailValid = checkEmail();
        let isFormValid = isEmailValid;

        if (isFormValid) {
            fetch(`/student/passwordReset`,{
                method:"POST",
                headers:{'Content-Type':'application/json; charset=UTF-8'},
                body:JSON.stringify({email:emailInput.value})
            }).then((res) => {
                return {
                    data:res.json(),
                    status:res.status
                }
            })
            .then(({data,status}) => {
                if (status === 200){
                    sessionStorage.setItem("UserEmail",emailInput.value)
                    window.location.href = "studentVerify_forgott.html";
                }else{
                    data.then(err => {
                        alert(err.message);
                    })
                }
            })
            .catch((e) => {
                console.log(e);
            })
           
        }
    }


    var emailInput = document.getElementById('email');
    var emailError = document.getElementById('emailError');

    emailInput.addEventListener('input', function(){
        checkEmail()
    })

    const checkEmail = () => {
        const email = emailInput.value.trim()
        if (!isRequired(emailInput.value)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email cannot be blank.';
            return false;
        } else if (!isEmailValid(email)){
            emailInput.classList.add('is-invalid');
            emailInput.classList.remove('is-valid');
            emailError.innerHTML = 'Email is not valid.';
            return false;
        } else{
            emailInput.classList.add('is-valid');
            emailInput.classList.remove('is-invalid');
            emailError.innerHTML = '';
            return true;
        }
    }
    
    const isRequired = value => value.trim() === ''? false : true;

    const isEmailValid = (email) => {
        const re = /^[0-9]{8}\.gcit@rub\.edu\.bt$/;
        return re.test(email)
    }
