const quantity = document.getElementById('quantity')
const pickupDate = document.getElementById('date')
const pickupTime = document.getElementById('time')
const phoneNO = document.getElementById('phoneno')
const description = document.getElementById('description')

window.onload = () => {
    fetch("/order")
        .then(res => res.json())
        .then(data => {
            displayOrders(data.message)
        })
        .catch(e => console.log(e))
}

const validateForm =() => {
    let isQuantityValid = checkQuantity(),
        isDateValid = checkDate(),
        isTimeValid = checkTime(),
        isPhoneValid = checkPhone(),
        isDescriptionValid = checkDescription();

    let isFormValid = isQuantityValid && isDateValid && isTimeValid && isPhoneValid && isDescriptionValid;

    if (isFormValid){
        var orderData = {
            quantity:quantity.value,
            date:pickupDate.value,
            time:pickupTime.value,
            number:phoneNO.value,
            description:description.value,
        }

        fetch("/order/create",{
            method:"POST",
            headers:{"Content-Type":"application/json; charset=UTF-8"},
            body:JSON.stringify(orderData)
        }).then((res) => {
            if (res.ok){
                alert("Order Placed Successfully")
                return res.json()
            }else{
                throw ""
            }
        }).then((data) => {
            orderData = {
                quantity: data.message.quantity,
                pickupDate: data.message.date.split('T')[0],
                pickupTime: convertTo12HourFormat(data.message.time),
                phoneNO: data.message.number,
                description: data.message.description,
                id:data.message._id,
                status:data.message.status
            }
            displayOrder(orderData)
        })
        .catch((e) => {
            console.log(e);
        })
    } 
    clearForm()
}

var quantityError = document.getElementById('quantityError');
var dateError = document.getElementById('dateError');
var timeError = document.getElementById('timeError');
var phoneNoError = document.getElementById('phoneNoError');
var descriptionError = document.getElementById('descriptionError');

quantity.addEventListener('input', function(){
    checkQuantity()
})
pickupDate.addEventListener('input', function(){
    checkDate()
})
pickupTime.addEventListener('input', function(){
    checkTime()
})
phoneNO.addEventListener('input', function(){
    checkPhone()
})
description.addEventListener('input',function(){
    checkDescription();
})

const checkQuantity = () => {
    if (!isRequired(quantity.value)){
        quantity.classList.add('is-invalid');
        quantity.classList.remove('is-valid');
        quantityError.innerHTML = 'Quantity cannot be blank.';
        return false;
    } 
    else if (!isQuantityValid(quantity.value)){
        quantity.classList.add('is-invalid');
        quantity.classList.remove('is-valid');
        quantityError.innerHTML = 'Number of quantiy of people should be between 10 and 100';
        return false;
    } 
    else{
        quantity.classList.add('is-valid');
        quantity.classList.remove('is-invalid');
        quantityError.innerHTML = '';
        return true;
    }
}

const isQuantityValid = () => {
    if (quantity.value >= 3 && quantity.value <= 100) {
        return true;
    }
    return false;
}

const isRequired = value => value.trim() === ''? false : true;

const checkDate = () => {
    if (!isRequired(pickupDate.value)){
        pickupDate.classList.add('is-invalid');
        pickupDate.classList.remove('is-valid');
        dateError.innerHTML = 'Date cannot be blank.';
        return false;
    } 
    else if (!isDateValid(pickupDate.value)){
        pickupDate.classList.add('is-invalid');
        pickupDate.classList.remove('is-valid');
        dateError.innerHTML = 'Date format is not correct';
        return false;
    } 
    else{
        pickupDate.classList.add('is-valid');
        pickupDate.classList.remove('is-invalid');
        dateError.innerHTML = '';
        return true;
    }
}
const isDateValid = (pickupDate) => {
    const re = /^\d{4}-\d{2}-\d{2}$/; // YYYY-MM-DD 
    return re.test(pickupDate);
}
const checkTime = () => {
    if (!isRequired(pickupTime.value)){
        pickupTime.classList.add('is-invalid');
        pickupTime.classList.remove('is-valid');
        timeError.innerHTML = 'Date cannot be blank.';
        return false;
    } 
    else if (!isTimeValid(pickupTime.value)){
        pickupTime.classList.add('is-invalid');
        pickupTime.classList.remove('is-valid');
        timeError.innerHTML = 'Time of quantiy of people should be between 10 and 100';
        return false;
    } 
    else{
        pickupTime.classList.add('is-valid');
        pickupTime.classList.remove('is-invalid');
        timeError.innerHTML = '';
        return true;
    }
}
const isTimeValid = (pickupTime) => {
    const re = /^\d{2}:\d{2}$/; // HH:MM
    return re.test(pickupTime);
}
const checkPhone = () => {
    if (!isRequired(phoneNO.value)){
        phoneNO.classList.add('is-invalid');
        phoneNO.classList.remove('is-valid');
        phoneNoError.innerHTML = 'Phone number cannot be blank.';
        return false;
    } 
    else if (!isPhoneValid(phoneNO.value)){
        phoneNO.classList.add('is-invalid');
        phoneNO.classList.remove('is-valid');
        phoneNoError.innerHTML = 'Phone number is not valid';
        return false;
    } 
    else{
        phoneNO.classList.add('is-valid');
        phoneNO.classList.remove('is-invalid');
        phoneNoError.innerHTML = '';
        return true;
    }
}
const isPhoneValid = (phoneNO) => {
    const re = /^(17|77)\d{6}$/;
    return re.test(phoneNO);
}
const checkDescription = () => {
    if (!isRequired(description.value)){
        description.classList.add('is-invalid');
        description.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description cannot be blank';
        return false;
    } 
    else if(!isDescriptionValid(description.value)){
        description.classList.add('is-invalid');
        description.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description should be between 3 and 300 characters';
        return false;
    }
     else {
        description.classList.add('is-valid');
        description.classList.remove('is-invalid');
        descriptionError.innerHTML = '';
        return true;
    }
}
const isDescriptionValid = () => {
    if (description.value.length >= 3 && description.value.length <= 300) {
        return true;
    }
    return false;
}

function displayOrders(orderCollection){
    orderCollection.forEach(item => {
        orderData = {
            quantity: item.quantity,
            pickupDate: item.date.split('T')[0],
            pickupTime: convertTo12HourFormat(item.time),
            phoneNO: item.number,
            description:item.description,
            id:item._id,
            status:item.status
        }
        displayOrder(orderData)
    });
}

function displayOrder(order) {
    const recentOrders = document.getElementById('recentOrders');

    const orderItem = document.createElement('div');
    orderItem.classList.add('order-item');
    orderItem.setAttribute("data-id", order.id);

    orderItem.innerHTML = `<h4 color="#E1DDBF">Order</h4><hr>
    <strong>Quantity:</strong> ${order.quantity} <br>
    <strong>Date:</strong> ${order.pickupDate} <br>
    <strong>Time:</strong> ${order.pickupTime} <br>
    <strong>Phone No:</strong> ${order.phoneNO} <br>
    <strong>Description:</strong> ${order.description} <br>
    <strong>Status:</strong> ${order.status} <br>`;

    const cancelButton = document.createElement('button');
    cancelButton.classList.add('cancel-btn');

    cancelButton.innerText = 'Cancel Order';
    cancelButton.addEventListener('click', function(event) {
        
        const id = event.target.parentNode.getAttribute("data-id");
        fetch(`/order/cancell/${id}`,{method:"PUT"})
            .then(res => {
                if (res.ok) {
                    alert("Order Cancelled")
                    orderItem.remove();
                }else{
                    throw ""
                }
            })
            .catch(e => console.log(e))
    });

    if (order.status == "pending" || order.status == "accepted"){
        orderItem.appendChild(cancelButton);
    }
    recentOrders.appendChild(orderItem);
}

// FORM RESETER
function clearForm() {
    document.getElementById('quantity').value='';
    document.getElementById('date').value='';
    document.getElementById('time').value='';
    document.getElementById('phoneno').value='';
    document.getElementById('description').value='';
}

// TIME CONVERTER
function convertTo12HourFormat(time24) {
    var timeParts = time24.split(':');
    var hours = parseInt(timeParts[0]);
    var minutes = parseInt(timeParts[1]);
  
    var period = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    if (hours > 12) {
      hours -= 12;
    } else if (hours === 0) {
      hours = 12;
    }
  
    // Format minutes to have leading zero if necessary
    var formattedMinutes = (minutes < 10) ? '0' + minutes : minutes;
  
    return hours + ':' + formattedMinutes + ' ' + period;
  }
  

