var addInformation = document.getElementById("sendInformationForm");
var subjectInput = document.getElementById('subject');
var descriptionInput = document.getElementById('body');
var subjectError = document.getElementById('subjectError');
var descriptionError = document.getElementById('bodyError');
var informationListTable = document.getElementById("tableInformation")
var selectedRow;

// DISPLAY ALL NOTICEBOARD  
window.onload = () => {
    fetch("/announcements")
    .then(res => res.json())
    .then(data => {
        data.message.forEach(element => {
            displayAllInformation(element) 
        });
    })
    .catch(e => console.log(e))
}

// FORM VALIDATION
const validateForm = (event) => {
    let isSubjectValid = checkSubject(),
        isDescriptionValid = checkDescription();
    let isFormValid = isSubjectValid && isDescriptionValid;

    if (isFormValid){
        const payload = {
            subject:subjectInput.value,
            body:descriptionInput.value,
        }
                fetch(`/announcement`, {
                    method: "POST",
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(payload)
                })
                .then(res => res.json())
                .then(data => {
                    alert("announcemnet send")
                    displayAllInformation(data.message)
                    resetform()
                    closeNoticeBoardDetails()
                })
                .catch(e => {console.log(e)})
        closeNoticeBoardDetails()
    } 
}

subjectInput.addEventListener('input', function(){
    checkSubject()
})

descriptionInput.addEventListener('input',function(){
    checkDescription();
})

const checkSubject = () => {
    if (!isRequired(subjectInput.value)){
        subjectInput.classList.add('is-invalid');
        subjectInput.classList.remove('is-valid');
        subjectError.innerHTML = 'Subject cannot be blank.';
        return false;
    } 
    else if (!isSubjectValid(subjectInput.value)){
        subjectInput.classList.add('is-invalid');
        subjectInput.classList.remove('is-valid');
        subjectError.innerHTML = 'Subject should be between 3 and 50 characters.';
        return false;
    } 
    else{
        subjectInput.classList.add('is-valid');
        subjectInput.classList.remove('is-invalid');
        subjectError.innerHTML = '';
        return true;
    }
}
const isSubjectValid = () => {
    if (subjectInput.value.length >= 3 && subjectInput.value.length <= 50) {
        return true;
    }
    return false;
}

const isRequired = value => value.trim() === ''? false : true;

const checkDescription = () => {
    if (!isRequired(descriptionInput.value)){
        descriptionInput.classList.add('is-invalid');
        descriptionInput.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description cannot be blank';
        return false;
    } 
    else if(!isDescriptionValid(descriptionInput.value)){
        descriptionInput.classList.add('is-invalid');
        descriptionInput.classList.remove('is-valid');
        descriptionError.innerHTML = 'Description should be between 3 and 300 characters';
        return false;
    }
     else {
        descriptionInput.classList.add('adInformation');
        descriptionInput.classList.remove('is-invalid');
        descriptionError.innerHTML = '';
        return true;
    }
}

const isDescriptionValid = () => {
    if (descriptionInput.value.length >= 3 && descriptionInput.value.length <= 300) {
        return true;
    }
    return false;
}

function resetform(){
    document.getElementById("subject").value = "";
    document.getElementById("body").value = "";
}
    
function displaySubjectAdd () {
    addInformation.style.visibility = "visible";
    addInformation.style.opacity = "1";
}

function closeNoticeBoardDetails(){
    resetform()
    addInformation.style.visibility = 'hidden';
    addInformation.style.opacity = 0;
}

function displayInformation() {
    var informationListTable = document.getElementById("tableInformation");
    var row = informationListTable.insertRow(informationListTable.length);

    var td=[]
    for (i=0; i<informationListTable.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = informationListTable.rows.length - 1;
    td[1].innerHTML = subjectInput.value;
    td[2].innerHTML = `<input type="button" value="View" id="view_btn" onclick="toggleSendUpdateButton(true,this)>`;
    td[3].innerHTML = '<input type="button" value="Delete" onclick="deleteRow(this)" id="delete_btn">';
}

// SHOW ALL INFORMATION
function displayAllInformation(item) {
    var informationListTable = document.getElementById("tableInformation");
    var row = informationListTable.insertRow(informationListTable.length);
    row.id = item._id

    var td=[]
    for (i=0; i<informationListTable.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = informationListTable.rows.length - 1;
    td[1].innerHTML = item.subject;
    td[2].innerHTML = '<input type="button" onclick="toggleSendUpdateButton(true,this)" value="View" id="view_btn">';
    td[3].innerHTML = '<input type="button" value="Delete" onclick="deleteInformation(this)" id="delete_btn">';
}

// POP UP
function displayInformationDetails(button) {
    fetch(`/announcement/${button.parentNode.parentNode.id}`).then(res => res.json()).then(data => {
        const response = data.message
        // Set the form values to the data from the table
        var form = document.getElementsByClassName("form")[0];
        form.id = response._id;
        console.log(form.id);
        subjectInput.value = response.subject;
        descriptionInput.value = response.body;
        displaySubjectAdd();
    }).catch(e => console.log(e))
}

function deleteInformation(event) {
    if (confirm("Are you sure you want to delete this information?")) {
        fetch(`/announcement/${event.parentNode.parentNode.id}`,{method:"DELETE"})
            .then(res => {
                if(res.ok){
                    informationListTable.deleteRow(event.parentNode.parentNode.rowIndex);
                    addInformation.style.visibility = "hidden";
                    addInformation.style.opacity = "0";
                }
            })
            .catch(e => console.log(e))
    }
}

function toggleSendUpdateButton(isUpdate,event) {
    const sendButton = document.getElementById('add');
    if (isUpdate) {
        selectedRow = event.parentNode.parentNode
        sendButton.innerHTML = 'Update';
        sendButton.setAttribute('onclick', `updateInformation("${event.parentNode.parentNode.id}")`);
        displayInformationDetails(event)
    } else {
        sendButton.innerHTML = 'Send';
        sendButton.setAttribute('onclick', 'validateForm(this)');
        displaySubjectAdd()
    }
}

function updateInformation(id){
        let isSubjectValid = checkSubject(),
            isDescriptionValid = checkDescription();
        let isFormValid = isSubjectValid && isDescriptionValid;
    
        if (isFormValid){
            const payload = {
                subject:subjectInput.value,
                body:descriptionInput.value,
            }
            
            fetch(`/announcement/${id}`, {
                method: "PUT",
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(payload)
            })
            .then(res => res.json())
            .then(data => {
                alert("Data Updated")
                selectedRow.cells[1].innerText = data.message.subject;
                closeNoticeBoardDetails()
            })
            .catch(e => console.log(e))
        } 
}