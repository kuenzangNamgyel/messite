const totalOrder = document.getElementById("totalOrder")
const totalUser = document.getElementById("totalUser")
const totalFeedback = document.getElementById("totalFeedback")

window.onload = () => {
    fetch("/stats")
        .then(res => res.json())
        .then(data => {
            totalOrder.innerHTML = data.message.totalOrder;
            totalFeedback.innerHTML = data.message.totalFeedback;
            totalUser.innerHTML = data.message.totalUser;
        })
        .catch(e => console.log(e))
}