//  BREAKFAST
const editButton = document.getElementById('edit-button');
const editForm = document.querySelector('.edit-form'); // Updated selector
const pictureInput = document.querySelector('.picture-input'); // Updated selector
const descriptionInput = document.querySelector('.description-input'); // Updated selector
const saveButton = document.querySelector('.save-button'); // Updated selector
const cancelButton = document.querySelector('.cancel-button'); // Updated selector
const profilePicture = document.getElementById('profile-picture');
const profileDescription = document.getElementById('profile-description');

// LUNCH
const editButton1 = document.getElementById('edit-button1');
const editForm1 = document.querySelector('.edit-form1'); // Updated selector
const pictureInput1 = document.querySelector('.picture-input1'); // Updated selector
const descriptionInput1 = document.querySelector('.description-input1'); // Updated selector
const saveButton1 = document.querySelector('.save-button1'); // Updated selector
const cancelButton1 = document.querySelector('.cancel-button1'); // Updated selector
const profilePicture1 = document.getElementById('profile-picture1');
const profileDescription1 = document.getElementById('profile-description1');

 // DINNER
const editButton2 = document.getElementById('edit-button2');
const editForm2 = document.querySelector('.edit-form2'); // Updated selector
const pictureInput2 = document.querySelector('.picture-input2'); // Updated selector
const descriptionInput2 = document.querySelector('.description-input2'); // Updated selector
const saveButton2 = document.querySelector('.save-button2'); // Updated selector
const cancelButton2 = document.querySelector('.cancel-button2'); // Updated selector
const profilePicture2 = document.getElementById('profile-picture2');
const profileDescription2 = document.getElementById('profile-description2');

window.onload = () => {

  breakfastImageUpdation()
  lunchImageUpdation()
  dinnerImageUpdation()

  fetch("/allMenu")
        .then(res => res.json())
        .then((data) => {
          profileDescription.innerHTML = data.message[1].description,
          profileDescription1.innerHTML = data.message[2].description,
          profileDescription2.innerHTML = data.message[0].description
        })
        .catch(e => console.log(e))
}

// IMAGE VALIDATOR
function checkFile(fileInput) {
  var file = fileInput.files[0]; // Get the selected file

  // Check file size
  var fileSizeInBytes = file.size;
  var maxSizeInBytes = 1024 * 1024; // 1MB
  if (fileSizeInBytes > maxSizeInBytes) {
    alert('File size exceeds the maximum limit of 1MB');
    fileInput.value = "";
    return;
  }

  // Check file format
  var allowedFormats = ['jpg', 'jpeg', 'png', 'gif'];
  var fileName = file.name.toLowerCase();
  var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

  if (!allowedFormats.includes(fileExtension)) {
    alert('Invalid file format. Allowed formats are: ' + allowedFormats.join(', '));
    fileInput.value = "";
    return;
  }``
}

// EDIT BUTTON
// Show the edit form when the edit button is clicked
editButton.addEventListener('click', () => {
  editForm.style.display = 'block';
  descriptionInput.value = profileDescription.innerText; // Pre-fill the description input
});

editButton1.addEventListener('click', () => {
  editForm1.style.display = 'block';
  descriptionInput1.value = profileDescription1.innerText; // Pre-fill the description input
});

editButton2.addEventListener('click', () => {
  editForm2.style.display = 'block';
  descriptionInput2.value = profileDescription2.innerText; // Pre-fill the description input
});

// SAVE BUTTON
saveButton.addEventListener('click', () => {
  var formData = new FormData();

  var description = descriptionInput.value;
  var imageFile = pictureInput.files[0];

  if (description.trim() === "" || pictureInput.value.trim() === "") {
    alert("Please fill in all the required fields.");
    return;
  }

    formData.append("description", description);
    formData.append("upload", imageFile);

    fetch("/menu/update/breakfast", {
      method: "PUT",
      body: formData
    })
    .then(response => {
      if (response.ok){
        fetch("/menu/mealInfo/breakfast")
          .then(res => res.json())
          .then(data => {
            breakfastImageUpdation()
            profileDescription.innerHTML = data.message.description
            editForm.style.display = 'none';
            alert("success")
          })
          .catch(e => console.log(e))
      }
    })
    .catch(error => {
      console.error("Error:", error);
    }); 
});

saveButton1.addEventListener('click', () => {
  var formData = new FormData();

  var description = descriptionInput1.value;
  var imageFile = pictureInput1.files[0];

  if (description.trim() === "" || pictureInput1.value.trim() === "") {
    alert("Please fill in all the required fields.");
    return;
  }

    formData.append("description", description);
    formData.append("upload", imageFile);

    fetch("/menu/update/lunch", {
      method: "PUT",
      body: formData
    })
    .then(response => {
      if (response.ok){
        fetch("/menu/mealInfo/lunch")
          .then(res => res.json())
          .then(data => {
            lunchImageUpdation()
            profileDescription1.innerHTML = data.message.description
            editForm1.style.display = 'none';
            alert("success")
          })
          .catch(e => console.log(e))
      }
    })
    .catch(error => {
      console.error("Error:", error);
    });
});

saveButton2.addEventListener('click', () => {
  var formData = new FormData();

  var description = descriptionInput2.value;
  var imageFile = pictureInput2.files[0];

  if (description.trim() === "" || pictureInput2.value.trim() === "") {
    alert("Please fill in all the required fields.");
    return;
  }

    formData.append("description", description);
    formData.append("upload", imageFile);

    fetch("/menu/update/dinner", {
      method: "PUT",
      body: formData
    })
    .then(response => {
      if (response.ok){
        fetch("/menu/mealInfo/dinner")
          .then(res => res.json())
          .then(data => {
            dinnerImageUpdation()
            profileDescription2.innerHTML = data.message.description
            editForm2.style.display = 'none';
            alert("success")
          })
          .catch(e => console.log(e))
      }
    })
    .catch(error => {
      console.error("Error:", error);
    });
});

document.addEventListener("DOMContentLoaded", function() {
  cancelButton.addEventListener('click', () => {
    editForm.style.display = 'none';
  });
  cancelButton1.addEventListener('click', () => {
    editForm1.style.display = 'none';
  });
  cancelButton2.addEventListener('click', () => {
    editForm2.style.display = 'none';
  });
});

function breakfastImageUpdation (){
  fetch('/menu/breakfast/image')
  .then(response => {
      if (!response.ok) {
      throw new Error('Request failed');
      }
      return response.blob();
  })
  .then(imageBlob => {
      const imageURL = URL.createObjectURL(imageBlob);
      profilePicture.src = imageURL;
  })
  .catch(error => {
      console.error(error);
  });
}

function lunchImageUpdation (){
  fetch('/menu/lunch/image')
  .then(response => {
      if (!response.ok) {
      throw new Error('Request failed');
      }
      return response.blob();
  })
  .then(imageBlob => {
      const imageURL = URL.createObjectURL(imageBlob);
      profilePicture1.src = imageURL;
  })
  .catch(error => {
      console.error(error);
  });
}

function dinnerImageUpdation (){
  fetch('/menu/dinner/image')
  .then(response => {
      if (!response.ok) {
      throw new Error('Request failed');
      }
      return response.blob();
  })
  .then(imageBlob => {
      const imageURL = URL.createObjectURL(imageBlob);
      profilePicture2.src = imageURL;
  })
  .catch(error => {
      console.error(error);
  });
}