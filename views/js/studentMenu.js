var breakfast_info = document.getElementById("breakfast_info")
var lunch_info = document.getElementById("lunch_info")
var dinner_info = document.getElementById("dinner_info")

var breakfast_img = document.getElementById("breakfast_img")
var lunch_img = document.getElementById("lunch_img")
var dinner_img = document.getElementById("dinner_img")

window.onload = () => {
    fetch('/menu/breakfast/image')
        .then(response => {
            if (!response.ok) {
            throw new Error('Request failed');
            }
            return response.blob();
        })
        .then(imageBlob => {
            const imageURL = URL.createObjectURL(imageBlob);
            breakfast_img.src = imageURL;
        })
        .catch(error => {
            console.error(error);
        });

    fetch('/menu/lunch/image')
        .then(response => {
            if (!response.ok) {
            throw new Error('Request failed');
            }
            return response.blob();
        })
        .then(imageBlob => {
            const imageURL = URL.createObjectURL(imageBlob);
            lunch_img.src = imageURL;
        })
        .catch(error => {
            console.error(error);
        });

    fetch('/menu/dinner/image')
        .then(response => {
            if (!response.ok) {
            throw new Error('Request failed');
            }
            return response.blob();
        })
        .then(imageBlob => {
            const imageURL = URL.createObjectURL(imageBlob);
            dinner_img.src = imageURL;
        })
        .catch(error => {
            console.error(error);
        });
    
    fetch("/menu")
        .then(res => res.json())
        .then((data) => {
            dinner_info.innerHTML = data.message[0].description,
            breakfast_info.innerHTML = data.message[1].description,
            lunch_info.innerHTML = data.message[2].description
        })
        .catch(e => console.log(e))
}