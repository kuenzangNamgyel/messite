var emailInput = document.getElementById('email')
var emailError = document.getElementById('emailError');

window.onload = () => {
    fetch("/admin/allEmails")
        .then(res => res.json())
        .then(data => {
            data.message.forEach(data2 => {
                console.log(data2);
                addAdminEmail(data2.email)
            })
        })
        .catch(e => console.log(e))
}

const validateForm = () => {
    let isEmailValid = checkEmail()
    let isFormValid = isEmailValid

    if (isFormValid){
        fetch("/admin/addEmail",{
            method:"POST",
            headers:{'Content-Type' : 'application/json; charset=UTF-8'},
            body:JSON.stringify({email:emailInput.value})
        })
        .then(res => {return {status:res.status,data:res.json()}})
        .then(res2 => {
            const status = res2.status
            const data = res2.data

            if (status === 200){
                alert("Member Added!")
                data.then(data2 => addAdminEmail(data2.message.email))
                closeEmailForm();
            }else{
                data.then((errData) => {
                    if (errData.message.code === 11000){
                        alert("Duplicate credential")
                    }
                })
            }
        })
        .catch(e => console.log(e))
        
    }
    resetForm()
}

emailInput.addEventListener('input', function(){
    checkEmail()
})

const checkEmail = () => {
    if (!isRequired(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email cannot be blank.';
        return false;
    } else if (!isEmailValid(emailInput.value)){
        emailInput.classList.add('is-invalid');
        emailInput.classList.remove('is-valid');
        emailError.innerHTML = 'Email is not valid.';
        return false;
    } else{
        emailInput.classList.add('is-valid');
        emailInput.classList.remove('is-invalid');
        emailError.innerHTML = '';
        return true;
    }
}
const isEmailValid = (email) => {
    const re = /^[a-zA-Z0-9._-]+\.gcit@rub\.edu\.bt$/;   
    return re.test(email)
}
const isRequired = value => value.trim() === ''? false : true;

function addAdminEmail(emailaddress) {
    var tableAdmin= document.getElementById('tableAdmin')
    var row = tableAdmin.insertRow(tableAdmin.length);

    var td=[]
    for (i=0; i<tableAdmin.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = tableAdmin.rows.length - 1;
    td[1].innerHTML = emailaddress;
    td[2].innerHTML = '<input type="button" onclick="deleteAdmin(this)" value="Delete" id="delete_btn">';  
}
function showAdmin(data) {
    // convert sudent data to json
    const admin = JSON.parse(data)
    // access table from html
    addAdminEmail(admin)    
}
function showStudents(dataList) {
    const admins = JSON.parse(dataList)

    admins.forEach(adm => {
        addAdminEmail(adm)
    })
}

function resetForm(){
    document.getElementById("email").value = "";
}

function deleteAdmin (r) {
    if (confirm("Are you sure you want to DELETE this?")){
        selectedRow = r.parentElement.parentElement;
        email = selectedRow.cells[1].innerHTML;
        var rowIndex = selectedRow.rowIndex;
        if (rowIndex>0) {
            fetch(`/admin/${email}`,{method:"DELETE"})
                .then(res => {
                    if (res.ok){
                        document.getElementById("tableAdmin").deleteRow(rowIndex);
                    }
                })
                .catch(e => {console.log(e)}) 
        }
        selectedRow = null;
    }
}

var addAdmin = document.getElementById("addForm")

function displayEmailAdd () {
    if (addAdmin.style.display === "none"){
        addAdmin.style.display = 'none'
    }
    addAdmin.style.display = 'block'
}
function closeEmailForm(){
    addAdmin.style.display = 'none'
}