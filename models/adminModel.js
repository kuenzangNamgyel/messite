const mongoose = require("mongoose")
const bcrypt = require("bcryptjs");
const studentUser = require("./studentModel");
const jwt = require("jsonwebtoken")

require("dotenv").config({ path: `./configuraiton/config` })

const adminSchema = new mongoose.Schema({
    userName:{
        type:String,
        required:[true,"You must provide a user name"],
        unique:true
    },
    password:{
        type:String,
        required:[true,"You must provide a password"],
        trim:true,
        minlength:2,
        Validate(value){
            if (value.toLowerCase().includes("Password")){
                throw new Error("Password cannot be 'Password !'")
            }
        }
    },
    tokens:[{
        token:{
            type:String,
            require:true
        }
    }]
});

adminSchema.methods.publicData = async function () {
    const admin = this
    // gives raw user data
    const adminObject = admin.toObject()

    delete adminObject.password
    delete adminObject.tokens

    return adminObject
}

adminSchema.methods.generateAuthToken = async function () {
    const admin = this 
    const token = jwt.sign({_id:admin._id.toString()},process.env.TOKEN_SIGNATURE)
    
    admin.tokens = admin.tokens.concat({token:token})
    await admin.save()

    return token
}

adminSchema.statics.findbyCredentials = async (userName,password) => {
    const admin = await adminUser.findOne({userName:userName})

    if(!admin){
        throw "Invalid Credential"
    }

    const isMatch = await bcrypt.compare(password,admin.password)
    if (!isMatch) {
        throw "Invalid Credential"
    }

    return admin
}

adminSchema.pre("save",async function(next){
    const admin = this

    if (admin.isModified("password")) {
        admin.password = await bcrypt.hash(admin.password,8)
    }

    next()
})

const adminUser = mongoose.model("adminUser",adminSchema)
module.exports = adminUser