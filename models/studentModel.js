const mongoose = require("mongoose")
const validator = require('validator')
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const adminUser = require("./adminModel")

require("dotenv").config({ path: './configuration/config.env' })

const studentSchema = new mongoose.Schema({
    enrollmentKey:{
        type:Number,
        required:true,
        unique:true
    },
    email:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error("Email is invalid")
            }
        }
    },
    password:{
        type:String,
        required:true,
        trim:true,
        minlength:8,
        validate(value){
            if (value.toLowerCase().includes("password")){
                throw new Error("Password cannot be 'Password !'")
            }
        },
    },
    confirmed:{
        type: Boolean,
        default:false
    },
    activationToken:{
        type:String,
        default:"nil"
    },
    passwordResetToken:{
        type:String,
        default:"nil"
    },
    tokens:[{
        token:{
            type: String,
            require: true
        }
    }]
})

studentSchema.methods.publicData = async function () {
    const user = this

    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    delete userObject.email
    delete userObject.activationToken
    delete userObject.passwordResetToken

    return userObject
}

studentSchema.methods.generateAuthToken = async function (){
    const user = this
    const token = jwt.sign({_id:user._id.toString()},process.env.TOKEN_SIGNATURE)

    user.tokens = user.tokens.concat({token:token})
    await user.save()

    return token
}

// custom function defined for the instance of the model 
studentSchema.methods.studentValidatorToken = async function () {
    const student = this
    const token = jwt.sign({_id:student._id.toString()},process.env.TOKEN_SIGNATURE,{expiresIn:"30 minutes"})
    
    // saving the token in student data activation link
   
    // Duplicate: if error only
    // student.activationToken = token
    // student.save()
    return token 
}

// Similar to receiver function in golang
// statics => 
studentSchema.statics.findByCredentials = async (email,password) => {
    const user = await studentUser.findOne({email:email})

    if (!user){
        throw "Invalid Credential"
    }

    const isMatch = await bcrypt.compare(password,user.password)
    if (!isMatch) {
        throw "Invalid Credential"
    }

    return user
}

// Setting up middleware for student model : pre method and post method
// pre(OnWhichMethod,funtionToRun)
studentSchema.pre("save",async function(next){
    const student = this

    // hashing password only if the user have created or updated their password
    if (student.isModified("password")) {
        student.password = await bcrypt.hash(student.password,8)
    }

    next()
})

const studentUser = mongoose.model("studentUser",studentSchema)
module.exports = studentUser