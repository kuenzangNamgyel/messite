const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
    quantity: {
      type: Number,
      required: true
    },
    date: {
      type: Date,
      required: true
    },
    time: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    number: {
        type: String,
        required: true
    },
    description: {
      type: String,
    },
    status: {
      type: String,
      required: true,
      default: 'pending',
      enum: ['pending', 'completed', 'cancelled','rejected','accepted']
    }
});

const order = mongoose.model("order",orderSchema)
module.exports = order