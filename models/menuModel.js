const mongoose = require("mongoose")

const menuSchema = new mongoose.Schema({
    title:{
        type:String,
        unique:true,
        required:true,
        trim:true,
        lowercase:true
    },
    description:{
        type:String,
        required:true,
    },
    image:{
        type:Buffer,
        required:true
    },
    special:{
        type:Boolean,
        default:false
    },
    // check for errors
    ratings:[{
        star:Number,
        postedby: {type:mongoose.Schema.Types.ObjectId,ref:"studentUser"},
    }],
    totalrating:{
        type:String,
        default:0,
    }
})

const menu = mongoose.model("menu",menuSchema)
module.exports = menu