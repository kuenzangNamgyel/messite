const mongoose = require("mongoose")

const feedbackSchema = new mongoose.Schema({
    senderEmail:{
        type:String,
        required:true,
        trim:true,
        lowercase:true,
    },
    subject:{
        type:String,
        trim:true,
        default:""
    },
    body:{
        type:String,
        required:true
    }
})

const feedback = mongoose.model("feedback",feedbackSchema)

module.exports = feedback