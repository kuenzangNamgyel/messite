const mongoose = require("mongoose")
const validator = require("validator")

const emailSchema = new mongoose.Schema({
    email:{
        type:String,
        required:[true,"You must provide a email"],
        unique:true,
        lowercase:true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error("Email is invalid ")
            }
        }
    }
})

const emailModel = mongoose.model("emailModel",emailSchema)
module.exports = emailModel