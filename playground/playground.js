// Then and Catch method
// user.save()
// .then(() => {
//         res.status(200).send({message:"Success"})
//     })
// .catch((error) => {
//     console.log(error)
//     res.status(400).send({message:"Invalid Input"})
// })

// Deleting collection in 
// router.post("/admin/signup",async (req,res) => {
//     const data = req.body
    
//     try {
//         adminModel.collection.drop()
//         // const admin = new adminModel(data)
//         // admin.save()
//         res.status(200).send("success: This is admin")
//     }catch(error){
//         res.status(400).send(error)
//     }  
// })

// Password hashing
// const bcrypt = require('bcryptjs')

// const myFunction = async() => {
//     const p = "abc"
//     const hashedp = await bcrypt.hash(p,8)

//     console.log(p);
//     console.log(hashedp);

//     // comparing hashed password 
//     // compare(plain,hashed)
//     const isMatch = await bcrypt.compare("hello",hashedp)
//     console.log(isMatch);
// }

// myFunction()

// sending email
// sgMail.send({
//     to:"kuenzangnamyel01@gmail.com",
//     from:"12220043.gcit@rub.edu.bt",
//     subject:"This is for messite development",
//     text:"It is working, hurray"
// })

// Webtoken
// const jwt = require("jsonwebtoken")

// const myfunction = async () => {
//     // Creation
//     // sign(object,string)
//     // token = header.payload.signature
//     const token = jwt.sign({_id:"abc123"},"messite",{expiresIn:"1 days"})
//     console.log(token);

//     // verification of token
//     const data = jwt.verify(token,"messite")
//     console.log(data);
// }
// myfunction()

// Email modification
// const x = {
//     name : "sonam",
//     age : 2
// }

// function person (y) {
//     console.log(y);
// }

// person(x)

// email
// remember the from email must be same as the send grid from email
// const userValidationEmail = (email,url) => {
//     sgMail.send({
//         to:email,
//         from: "12220043.gcit@rub.edu.bt",
//         subject: "Account verification",
//         text:"click on the link below to activate your account.The token expires with 1 day so by the time you activate If the token is expired then go to the login page and use your email and password to sign in. Then the system will automatically send you another link to activate",
//         html:`Please click this email to confirm your email:<a href="${url}">${url}<a> 
//         <p>click on the link below to activate your account.The token expires with 1 day so by the time you activate If the token is expired then go to the login page and use your email and password to sign in. Then the system will automatically send you another link to activate</p>`
//     })
// }

// const path = require("path")
// console.log(__dirname);
// // console.log(__filename);
// console.log(path.join(__dirname,"../view"));

// Middleware : should be above all use() method
// app.use((req,res,next) => {
//     if(req.method === "POST"){
//         res.status(400).send("Get requests are disabled.")
//     }else{
//         next()
//     }
//    next()
// })


// Playground
// const multer = require("multer")

// // multer configuration
// const upload = multer({
//     dest:"images"
// })

// // upload.single() is a middleware which handles single file upload.
// app.post("/upload",upload.single('upload'),(req,res) => {
//     res.send()
// }) 

// cb(new Error("Please upload an image."))
// cb(undefined,true)
// cb(undefined,false)

// const token = req.header("Authorization").replace("Bearer ","")
// const decoded = jwt.verify(token,process.env.TOKEN_SIGNATURE)

// // const checking for admin with the id and also checking if token is present.
// const admin = await adminModel.findOne({_id:decoded._id,"tokens.token":token})
// if (!admin){
//      throw ""
// }

// req.token = token
// req.admin = admin
// next()