const { models } = require("mongoose")
const studentModel = require("../models/studentModel")
const feedbackModel = require("../models/feedbackModel")
const {userValidationEmail} = require("../utility/email")
const jwt = require("jsonwebtoken")
const orderModel = require("../models/orderModel")
const announcementModel = require("../models/announcementModel")
const menuModel = require("../models/menuModel")

require("dotenv").config({ path: './configuration/config.env' })

const registerUser = async (req,res) => {
    try {
        const student = new studentModel(req.body)

        // Checks for Duplication
        const isPresent = await studentModel.findOne({email:student.email})
        if(!(isPresent)){
            await student.save()
        }else{
            throw {code:11000}
        }
     
        // generateAuthToken is a custom function defined for on the instance on the model which is the user in this case
        const token = await student.studentValidatorToken()
        const url = `${process.env.HOSTING}/userConfirmation/${token}`
        const emailContent = {
            to : student.email,
            url: url,
            subject: "Account Activation Link",
            text : "To activate your account, click the following link.The token has a 1-day expiration date, so when you activate If the token has run out of time, visit the login page and sign in with your email and password. A second link to activate will then be sent to you automatically by the system."
        }
        userValidationEmail(emailContent)

        // userValidationEmail(student.email,url)

        student.activationToken = token
        await student.save()

        const AuthToken = await student.generateAuthToken()
        res.status(200).send({message:"To comple the signup process activate your account through the link we have send you in your email.",token:AuthToken})
    }
    catch (error) {
        res.status(400).send({message:error})
    }
}

const loginUser = async (req,res) => {
    try {
        // findByCredential is a custom function on the model of student
        const user = await studentModel.findByCredentials(req.body.email,req.body.password)
        const AuthToken = await user.generateAuthToken()

        if (!user.confirmed){
            // Backup account activation link
            try{
                const tokenUser = jwt.verify(user.activationToken,process.env.TOKEN_SIGNATURE)
                if (tokenUser){
                    throw "verify your account, we have already send you a activation link in your email"
                }
            }catch(error){
                if (error.message == "jwt expired"){
                    const token = await user.studentValidatorToken()
                    const url = `${process.env.HOSTING}/userConfirmation/${token}`
                    const emailContent = {
                        to : user.email,
                        url: url,
                        subject : "Account Activation Link",
                        text : "To activate your account, click the following link.The token has a 1-day expiration date, so when you activate If the token has run out of time, visit the login page and sign in with your email and password. A second link to activate will then be sent to you automatically by the system."
                    }
                    userValidationEmail(emailContent)

                    throw "verify your account,we have send you a new account activation link through the email."
                }else{
                    throw error
                }
            }
        }else{
            res.cookie(`session_id`, AuthToken)
            res.status(200).send({message:await user.publicData(),token:AuthToken})
        }
        
    }catch(e){
        res.status(400).send({message:e})   
    }
}

const userConfirmation = async (req,res) => {
    try {
        const user = jwt.verify(req.params.token,process.env.TOKEN_SIGNATURE)
        await studentModel.findOneAndUpdate({_id:user._id},{confirmed:true})
        res.redirect(`${process.env.HOSTING}/studentLogin.html`);
    }catch(error){
        res.status(400).send({message:error})
    }
}

const passwordResetHandler = async (req,res) => {
    try{
        const student = await studentModel.findOne({email:req.body.email})

        if (!student){
            throw 'Invalid credential'
        }

        if (!student.confirmed){
            throw 'Please activate your account inorder to reset the password, we have already send you the activation link through your email'
        }
        
        try {
            if (student.passwordResetToken != "nil"){
                const {_id} = jwt.verify(student.passwordResetToken,process.env.TOKEN_SIGNATURE)
                if (_id){
                    throw `Your current password reset link is valid, please use the link that we have already send to your email.`
                } 
            }else{
                throw `users first time`
            }
        }catch(e){
            if (e.message == "jwt expired" || student.passwordResetToken == "nil"){
                const token = await student.studentValidatorToken()
                // const url = `${process.env.HOSTING}/student/passwordResetForm/${token}`
                const url = `${process.env.HOSTING}/studentRewriting_pw.html?token=${token}`

                // userValidationEmail(student.email,url)
                const emailContent = {
                    to : student.email,
                    url: url,
                    subject : "Password Reset Link",
                    text : "Please click on the link to reset your password. If the link expires, you must restart the entire procedure."
                }
                userValidationEmail(emailContent)

                student.passwordResetToken = token
                await student.save()

                res.status(200).send({message:"We have send a password reset link to your email, use it to reset your passwrod."})
            }else{
                throw e
            }
        } 
    }catch(e){
        res.status(400).send({message:e})
    }
}

const passwordResetForm = async (req,res) => {
    try{
        const {_id} = jwt.verify(req.params.token,process.env.TOKEN_SIGNATURE)
        const student = await studentModel.findOne({_id:_id})
        
        student.password = req.body.password
        await student.save()

        res.status(200).send({message:"successfully changed the password"})

        // After the integration of frontend
        // return res.redirect("http://localhost:3000/student/login")
    }catch(e){
        res.status(400).send({message:e})
    }
}

const passwordResetInside = async (req,res) => {
    try{
        req.student.password = req.body.password
        await req.student.save()

        res.status(200).send({message:"successfully changed the password"}) 
    }catch(e){
        res.status(400).send({message:e})
    }
}

const getStudent = async (req,res) => {
    try{
        // const user = await studentUser.findOne({email:email})
        const student = await studentModel.findOne({email:req.student.email})
        res.status(200).send({message:{
            email:student.email,
            enrollment:student.enrollmentKey
        }})
    }
    catch(e){
        res.status(400).send("Unsuccessful")
    }
}

const logoutHandler = async (req,res) => {
    try{
        req.student.tokens = req.student.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.student.save()

        res.status(200).send()
    }catch(e){
        res.status(500).send()
    }
}

const createFeedback = async (req,res) => {
    try{
        const feedback = new feedbackModel(req.body)
        feedback.senderEmail = req.student.email
        await feedback.save()

        res.status(200).send({message:feedback})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const createOrder = async (req,res) => {
    try {
        const order = new orderModel(req.body)
        order.email = req.student.email
        await order.save()

        res.status(200).send({message:order})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getOrder = async (req,res) => {
    try {
        const orders = await orderModel.find({email:req.student.email,status: { $in: ["pending", "rejected","accepted"] }})
        res.status(200).send({message:orders})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const cancellOrder = async (req,res) => {
    try{
        const order = await orderModel.findOne({_id:req.params.id})
        order.status = "cancelled"
        order.save()
        res.status(200).send({message:order})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getAllAnnouncement = async (req,res) => {
    try{
        const announcement = await announcementModel.find({})
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getAllMenu = async (req,res) => {
    try{
        const menu = await menuModel.find({})
        res.status(200).send({message:menu})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const rating = async (req,res) => {
    try{
        const ratedBy = req.student._id
        const {rating,meal} = req.body
        
        const mealProduct = await menuModel.findOne({title:meal}).exec()

        if (mealProduct) {
            let alreadyRated = mealProduct.ratings.find(
              (doc) => doc.postedby.toString() === ratedBy.toString()
            );

            if (alreadyRated) {
                const updateRating = await menuModel.updateOne(
                {
                    "ratings":{ $elemMatch: alreadyRated }
                },
                {
                    $set:{"ratings.$.star":rating}
                },
                {
                    new:true
                })
            } else {
                const rateProduct = await menuModel.findOneAndUpdate(
                    {title:meal},
                    {
                        $push:{
                            ratings:{
                                star:rating,
                                postedby:ratedBy,
                            },
                        },
                    },
                    {new:true}
                );
            }
        }else {
            res.status(404).send("Meal not found");
        }

        const getallratings = await menuModel.findOne({title:meal}).exec()
        let totalrating = getallratings.ratings.length;

        // map() & reduce(callback function)
        let ratingsum = getallratings.ratings.map((item) => item.star).reduce((prevValue,currValue) => prevValue + currValue,0)

        let actualRating = Math.round(ratingsum/totalrating)

        let updatedTotalRating = await menuModel.findOneAndUpdate({title:meal},{totalrating:actualRating},{new:true})

        res.status(200).json(updatedTotalRating)
        
    }catch(e){
        res.status(400).send(e)
    }
}

module.exports = {
    registerUser,
    loginUser,
    userConfirmation,
    passwordResetHandler,
    passwordResetForm,
    getStudent,
    logoutHandler,
    createFeedback,
    passwordResetInside,
    createOrder,
    getOrder,
    cancellOrder,
    getAllAnnouncement,
    getAllMenu,
    rating
} 