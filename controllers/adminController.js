const adminModel = require("../models/adminModel")
const emailModel = require("../models/adminEmails")
const {adminPasswordRecoveryEmail, userValidationEmail,orderCompletionEmail} = require("../utility/email")
const feedbackModel = require("../models/feedbackModel")
const menuModel = require("../models/menuModel")
const sharp = require("sharp")
const orderModel = require("../models/orderModel")
const announcementModel = require("../models/announcementModel")
const userModel = require("../models/studentModel")

// ADMIN CONTROLLER
const registration = async (req,res) => { 
    try {
        const admin = new adminModel(req.body)
        await admin.save()
        
        const AuthToken = await admin.generateAuthToken()
        res.status(200).send({message:"Success",token:AuthToken})
    }
    catch(error){
        res.status(400).send({message:"Invalid Input"})
    }  
}

const adminLogin = async (req,res) => {
    try{
        const admin = await adminModel.findbyCredentials(req.body.userName,req.body.password)
        const token = await admin.generateAuthToken()
        res.cookie(`session_id`, token)
        res.status(200).send({message: await admin.publicData(),token:token})
    }catch(error) {
        res.status(400).send({message : error})
    }
}

const passwordRecoveryHandler = async (req,res) => {
    try{
        const isFound = await emailModel.findOne({email : req.body.email})

        if(isFound){
            adminPasswordRecoveryEmail(req.body.email)
            res.status(200).send({email : req.body.email})
        }else{
            throw 'notFound'
        }

        
    }catch(e){
        var statusCode = 400
        message = e 
        if (e === "notFound"){
            statusCode = 404
            message = "Email not found"
        }
        res.status(statusCode).send({message:message})
    }
}

const adminLogoutHandler = async(req,res) => {
    try{
        req.admin.tokens = req.admin.tokens.filter((token) => {
            return token.token !== req.token
        })
        
        await req.admin.save()

        res.status(200).send()
    }catch(e){
        res.status(500).send(e)
    }
}

// EMAIL FUNCTIONALITY

const addEmailHandler = async (req,res) => {
    try{
        const email = new emailModel(req.body)
        await email.save()
        res.status(200).send({message:email})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const getAllEmail = async (req,res) => {
    try{
        const emails = await emailModel.find({})
        res.status(200).send({message:emails})
    }catch(e) {
        res.status(400).send({message:"unsucces"})
    }
}

const getEmail = async (req,res) => {
    try{
        const email = await emailModel.findOne({email:req.params.email}) 
        res.status(200).send({message:email})
    }catch(e){
        res.status(400).send({message:"unsuccessful"})
    }
}

const deleteEmail = async (req,res) => {
    try{
        const email = await emailModel.findOneAndDelete({email:req.params.email}) 
        res.status(200).send({message:email})
    }catch(e){
        res.status(400).send({message:"unsuccessful"})
    }
}

const updateEmail = async (req,res) => {
    try{
        const email = await emailModel.findOneAndUpdate({email:req.params.email},{email:req.body.email}) 
        res.status(200).send({message:email})
    }catch(e){
        res.status(400).send({message:"unsuccessful"})
    }
}

// FEEDBACK FUNCTIONALITY

const getFeedback = async(req,res) => {
    try{ 
        if (!req.params.feedbackId){
            throw "Please provide a email to search"
        }
        const feedback = await feedbackModel.findOne({_id:req.params.feedbackId}) 
        res.status(200).send({message:feedback})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const getAllFeedback = async(req,res) => {
    try{
        const feedbacks = await feedbackModel.find({})
        res.status(200).send({message:feedbacks})
    }catch(errror) {
        res.status(400).send({message:"Unsuccessful"})
    }
}

const deleteFeedback = async (req,res) => {
    try{
        if (!req.params.feedbackId){
            throw "Please provide a email delete"
        }
        const feedback = await feedbackModel.findOneAndDelete({_id:req.params.feedbackId})
        res.status(200).send({message:feedback})
    }catch(errror) {
        res.status(400).send({message:"Unsuccessful"})
    }
} 

// MENU FUNCTIONALITY
const addMenu = async (req,res) => {
    try{
        const buffer = await sharp(req.file.buffer).resize({width:250,height:250}).png().toBuffer()
        // const image = req.file.buffer
        const data = {
            title:req.body.title,
            description:req.body.description,
            image:buffer
        }
        
        const menu = new menuModel(data)
        
        await menu.save()

        res.status(200).send({message:"success"})
    }catch(error) {
        res.status(400).send({message:error})
    }
}

const getMenuImage = async (req,res) => {
    try{
        const meal = await menuModel.findOne({title:req.params.meal})
        if (!meal || !meal.image){
            throw ""
        }
        res.set("Content-Type","image/png")
        res.send(meal.image)
    }catch(errror) {
        res.status(404).send({message:"Unsuccessful"})
    }
}

const getMenuInfo = async (req,res) => {
    try{
        const meal = await menuModel.findOne({title:req.params.meal}).select('title description special')
        res.status(200).send({message:meal})
    }catch(errror) {
        res.status(400).send({message:"Failed"})
    }
}

const getAllAdminMenu = async (req,res) => {
    try{
        const menu = await menuModel.find({})
        res.status(200).send({message:menu})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const updateMenu = async (req,res) => {
    try{
        const buffer = await sharp(req.file.buffer).resize({width:250,height:250}).png().toBuffer()

        const menu = await menuModel.findOneAndUpdate(
            {title:req.params.meal},
            {description:req.body.description, image:buffer, special:req.body.special}
        )

        res.status(200).send({message:menu})
    }catch(errror) {
        res.status(400).send({message:"Failed"})
    }
}

const getOrders = async (req,res) => {
    try{
        const orders = await orderModel.find({})
        res.status(200).send({message:orders})
    }catch(e) {
        res.status(400).send({message:"unsuccess"})
    }
}

const getcatagoryOrder = async (req,res) => {
    try{
        const orders = await orderModel.find({status:req.params.type})
        res.status(200).send({message:orders})
    }catch(e) {
        res.status(400).send({message:"unsuccess"})
    }
}

const orderStatusUpdate = async (req,res) => {
    try{
        const order = await orderModel.findOne({_id:req.params.id})
        order.status = req.query.status
        order.save()

        if (req.query.status == "completed"){
            orderCompletionEmail(order.email)
        }

        res.status(200).send({message:order})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getSpecificOrder = async (req,res) => {
    try {
        const orders = await orderModel.findOne({_id:req.params.id})
        res.status(200).send({message:orders})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const deleteOrder = async (req,res) => {
    try {
        const orders = await orderModel.findOneAndDelete({_id:req.params.id})
        res.status(200).send({message:orders})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const createAnnouncement = async (req,res) => {
    try{
        const announcement = new announcementModel(req.body)
        await announcement.save()
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getAnnouncement = async (req,res) => {
    try{
        const announcement = await announcementModel.findOne({_id:req.params.id})
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const updateAnnouncement = async (req,res) => {
    try{
        const announcement = await announcementModel.findOneAndUpdate(
            {_id:req.params.id},
            {subject:req.body.subject,body:req.body.body}
        )
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const getAllAnnouncement = async (req,res) => {
    try{
        const announcement = await announcementModel.find({})
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const deleteAnnouncement = async (req,res) => {
    try{
        const announcement = await announcementModel.findOneAndDelete({_id:req.params.id})
        res.status(200).send({message:announcement})
    }catch(e){
        res.status(400).send({message:"unsuccess"})
    }
}

const getStats = async (req,res) => {
    try{
        const data = {
            totalUser: await userModel.countDocuments({confirmed:true}),
            totalFeedback: await feedbackModel.countDocuments(),
            totalOrder: await orderModel.countDocuments({ status: 'accepted' })
        }
        res.status(200).send({message:data})
    }catch(e){
        res.status(400).send({message:e})
    }
}

module.exports = {
    registration,
    adminLogin,
    passwordRecoveryHandler,
    addEmailHandler,
    adminLogoutHandler,
    getFeedback,
    getAllFeedback,
    deleteFeedback,
    getAllEmail,
    getEmail,
    deleteEmail,
    updateEmail,
    addMenu,
    getMenuImage,
    getMenuInfo,
    updateMenu,
    getOrders,
    orderStatusUpdate,
    getSpecificOrder,
    deleteOrder,
    createAnnouncement,
    getAnnouncement,
    updateAnnouncement,
    getAllAnnouncement,
    deleteAnnouncement,
    getStats,
    getAllAdminMenu,
    getcatagoryOrder
}