const jwt = require("jsonwebtoken")
const adminModel = require("../models/adminModel")
const studentModel = require("../models/studentModel")

require("dotenv").config({ path: '../configuration/config.env' })

const adminAuth = async (req,res,next) => {
   try {
     const {cookies} = req; 
     var token = ""
     if('session_id' in cookies){
         token = cookies.session_id
     }else{
          throw ""
     }

     const decoded = jwt.verify(token,process.env.TOKEN_SIGNATURE)

     // const checking for admin with the id and also checking if token is present.
     const admin = await adminModel.findOne({_id:decoded._id,"tokens.token":token})
     if (!admin){
          throw ""
     }

     req.token = token
     req.admin = admin
     next()
   }catch(e) {
        res.status(401).send({error:"Please Authenticate."})
   }
}

const studentAuth = async (req,res,next) => {
     try{
          const {cookies} = req; 
          var token = ""
          if('session_id' in cookies){
               token = cookies.session_id
          }else{
               throw ""
          }

          const decoded = jwt.verify(token,process.env.TOKEN_SIGNATURE)

          const student = await studentModel.findOne({_id:decoded._id,"tokens.token":token})
          
          if (!student) {
               throw ""
          }else{
               req.token = token
               req.student = student
               next()
          }
     }catch(e) {
          res.status(401).send({error:"Please Authenticate."})
     }
}

module.exports = {adminAuth,studentAuth}