// Server
const express = require("express")
const app = express()
const path = require("path")
const cookieParser = require(`cookie-parser`)

app.set("view engine","html")
app.set('views',path.join(__dirname,'views'))
app.use(express.json())
app.use(express.static(path.join(__dirname,"views")))

// Database
require("./database/mongoose")

// Routes
const studentRouter = require("./routes/studentRoute")
const adminRouter = require("./routes/adminRoute")

app.use(cookieParser())

app.get('/',(req,res) => {
    const filePath = path.join(__dirname,"views","Index.html")
    res.sendFile(filePath);
})

app.use(studentRouter)
app.use(adminRouter)

// 404 PAGE
app.get("*",(req,res) => {
    const filePath = path.join(__dirname, 'views','404.html');
    res.sendFile(filePath);
})

module.exports = app