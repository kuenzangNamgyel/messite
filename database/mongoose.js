const mongoose = require("mongoose")

require("dotenv").config({ path: './configuration/config.env' })
const DB = process.env.DATABASE_LOCAL

mongoose.connect(DB)
    .then((con) =>{
    console.log(con.connections)
    console.log("DB connect successful")
    })
    .catch((error) => {
        console.log(error)
    });

