// SERVER
const express = require('express')
const router = new express.Router()

// File Handler
const multer = require("multer")
const upload = multer({
    limits:{
        fileSize: 1000000
    },
    fileFilter(req,file,cb){
        if (!file.originalname.match(/\.(jpeg|jpg|png|gif|bmp|tiff|tif|webp|svg)$/)){
            return cb(new Error("Please upload an image"))
        }

        cb(undefined,true)
    }
})

// MIDDLEWARE
const {adminAuth} = require("../middleware/auth")

// CONTROLLER
const controller = require("../controllers/adminController")

// ADMIN ROUTES
router.post("/admin/signup",controller.registration)
router.post("/admin/login",controller.adminLogin)
router.post("/admin/passwordRecovery",controller.passwordRecoveryHandler)
router.post("/admin/logout",adminAuth,controller.adminLogoutHandler)

// EMAIL ROUTES
router.post("/admin/addEmail",adminAuth,controller.addEmailHandler)
router.get("/admin/allEmails",controller.getAllEmail)
router.get("/admin/:email",controller.getEmail)
router.delete("/admin/:email",adminAuth,controller.deleteEmail)
router.put("/admin/:email",adminAuth,controller.updateEmail)

// FEEDBACK ROUTES
router.get("/feedback/:feedbackId",adminAuth,controller.getFeedback)
router.get("/feedbacks",adminAuth,controller.getAllFeedback)
router.delete("/feedback/:feedbackId",adminAuth,controller.deleteFeedback)

// MENU ROUTES
// forth argument is for handling error. 
router.post("/menu/image",adminAuth,upload.single("upload"),controller.addMenu,(error,req,res,next) => {
    res.status(400).send({error:error.message})
})
router.get("/menu/:meal/image",controller.getMenuImage)
router.get("/menu/mealInfo/:meal",adminAuth,controller.getMenuInfo)
router.put("/menu/update/:meal",upload.single("upload"),controller.updateMenu,(error,req,res,next) => {
    res.status(400).send({error:error.message})
})
router.get("/allMenu",adminAuth,controller.getAllAdminMenu)

router.get("/orders",adminAuth,controller.getOrders)
router.get("/orders/:type",adminAuth,controller.getcatagoryOrder)
router.put("/order/status/:id",adminAuth,controller.orderStatusUpdate)
router.get("/order/specific/:id",adminAuth,controller.getSpecificOrder)
router.delete("/order/:id",adminAuth,controller.deleteOrder)

router.post("/announcement",adminAuth,controller.createAnnouncement)
router.get("/announcement/:id",adminAuth,controller.getAnnouncement)
router.put("/announcement/:id",adminAuth,controller.updateAnnouncement)
router.get("/announcements",adminAuth,controller.getAllAnnouncement)
router.delete("/announcement/:id",adminAuth,controller.deleteAnnouncement)

router.get("/stats",adminAuth,controller.getStats)

module.exports = router