const express = require("express")
const router = new express.Router()

const {studentAuth} = require("../middleware/auth")

const controller = require("../controllers/studentController")

router.post("/student/signup",controller.registerUser)
router.post("/student/login",controller.loginUser)
router.get("/userConfirmation/:token",controller.userConfirmation)
router.post("/student/passwordReset",controller.passwordResetHandler)
router.post("/student/passwordResetForm/:token",controller.passwordResetForm)
router.put("/student/passwordReset",studentAuth,controller.passwordResetInside)
router.get("/student",studentAuth,controller.getStudent)
router.post("/student/logout",studentAuth,controller.logoutHandler)

router.post("/feedback/create",studentAuth,controller.createFeedback)

router.post("/order/create",studentAuth,controller.createOrder)
router.get("/order",studentAuth,controller.getOrder)
router.put("/order/cancell/:id",studentAuth,controller.cancellOrder)

router.get("/announcements/all",studentAuth,controller.getAllAnnouncement)

router.get("/menu",studentAuth,controller.getAllMenu)

router.put("/rating",studentAuth,controller.rating);

module.exports = router