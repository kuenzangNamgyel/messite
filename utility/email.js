const sgMail = require("@sendgrid/mail")
require("dotenv").config({ path: './configuration/config.env' })

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

// remember the from email must be same as the send grid from email
const userValidationEmail = (content) => {
    sgMail.send({
        to:content.to,
        from: "12220043.gcit@rub.edu.bt",
        subject: content.subject,
        text:`${content.text}`,
        html:`<a href="${content.url}">${content.url}<a> <br> <p>${content.text}</p>`
    })
}

const adminPasswordRecoveryEmail = (email) => {
    sgMail.send({
        to: email,
        from:"12220043.gcit@rub.edu.bt",
        subject:"Password For Messite",
        text:`Please use this password to login . "${process.env.ADMIN_PASSWORD}"`
    })
}

const orderCompletionEmail = (email) => {
    sgMail.send({
        to:email,
        from:"12220043.gcit@rub.edu.bt",
        subject:"Order Completion",
        text:"Your order has been completed."
    })
}

module.exports = {
    userValidationEmail,
    adminPasswordRecoveryEmail,
    orderCompletionEmail
}